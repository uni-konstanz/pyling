# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Classes and Functions that provide syllabification

All syllabifiers are derived from the abstract base class
ABCSyllabifier which defines a common interface. A syllabifier
basically is just a wrapper class for a syllabify function
which is called directly by calling the syllabifier itself.
This has the advantage that syllabification can be ensured
through type or instance checking of syllabifier objects. This
is more robust than using syllabify functions directly.
Assumes normalized input and does not ignore case.

Implements the following classes:
ABCSyllabifier          --          abstract base class for all syllabifiers
SimpleSyllabifier       --          only works on CV sequences
AutoSyllabifier         --          identifies C clusters

Syllabifiers prove a function syllabifier that just checks the input
parameters and then calls the abstract method _syllabify on the input string.
This must be implement in child classes. The function __call__() is a wrapper
for syllabify() that takes tuples of words as parameter and calls syllabify()
on each of the words.
"""

from abstract import *

__all__ = ["ABCSyllabifier","SimpleSyllabifier","AutoSyllabifier"]

class ABCSyllabifier (Abstract,object):
    """Abstract Base Class for Syllabifiers

    Defines a common interface. A syllabifier takes a list
    of vowels as arguments for instantiation. If additional
    arguments must be set, reasonable defaults should be provided.
    The syllabifier can be called directly via the __call__ funtion.
    It takes a tuple of words as argument. Each word is then syllabified
    and a dictionary of words and syllables returned.
    """
    _name_ = "ABCSyllabifier"
    
    def __init__ (self,*vowels):
        """Instantiate a Syllabifier

        @vowels         --      a list of vowels
        """
        self._vowels = vowels

    def __repr__ (self):
         return "%s(%s)" % (self._name_,id(self))
        
    def __call__ (self,*words):
        """__call__ (*words) -> { word : [syllables] }

        Does the syllabification of the input words.
        """
        syllables = {}
        for word in words:
            try: syllables[word]
            except (KeyError): syllables[word] = self.syllabify(word,0,len(word))
        return syllables

    def syllabify (self,p,i=0,j=0):
        """syllabify(p,i,j) -> string, syllabify the input string p from position i to j"""
        if j <= i+1: return p
        return self._syllabify(p[i:j])

    def _syllabify (self,p):
        abstract()


class SimpleSyllabifier (ABCSyllabifier):
    """Identify syllables by vowels.

    This syllabifier just searches for vowels with preceding
    consonants. The word is split into syllables right before
    CV.
    """
    _name_ = "SimpleSyllabifier"
    
    def _syllabify (self,p):
        cur = p[0]
        iscluster = False if cur not in self._vowels else True
        for c in p[1:]:
            if c in self._vowels:
                if iscluster:
                    if cur[-1] not in self._vowels:
                        cur = cur[:-1]+ '.'+cur[-1]
                iscluster = True
            cur += c
        return cur.split('.')


class AutoSyllabifier (ABCSyllabifier):
    """Identify syllables by consonant clusters.

    Searches for initial (onset) and final (coda) consonant clusters.
    Words are split on the longest possible onsets.
    """
    _name_ = "AutoSyllabifier"
