# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Classes and Functions for stemming

All Stemmers are derived fromt he abstract base class
ABCStemmer which defines a common interface. All
Stemmer classes should begin with country code
followed by '_' an identifying stemmer name and '_Stemmer'.
The stemmer assumes normalized input and does not ignore case.

Implements the following classes:
ABCStemmer               --          abstract base stemmer class
DE_PlGen_Stemmer         --          german plural and case stemmer
DE_Porter_Stemmer        --          german porter stemmer
EN_Porter_Stemmer        --          english porter stemmer
"""

from abstract import *

__all__ = ["ABCStemmer","DE_PlGen_Stemmer","DE_Porter_Stemmer","EN_Porter_Stemmer"]


class ABCStemmer (Abstract,object):
    """Abstract Base Class for all stemmers

    Implements a common interface. A Stemmer is language specific.
    It can be called directly by providing a __call__ function which
    is a wrapper for stem() and iterates over all words in the input.

    Members:
    _b          --          buffer for the word to be stemmed
    _i          --          starting position in string
    _j          --          end position in string
    """
    _name_ = "ABCStemmer"
    
    def __init__ (self):
        self._b = ""
        self._i = 0
        self._j = 0
    
    def __call__ (self,*words):
        """__call__ (*words) -> { word : stem }

        The call to a stemmer is a wrapper for stem(). It iterates
        over all words and calls the stem() function for each.
        """
        stems = {}
        for word in words:
            try: stems[word]
            except (KeyError): stems[word] = self.stem(word,0,len(word))
        return stems

    def __repr__ (self):
        return "%s(%s)" % (self._name_,id(self))

    def stem (self,p,i=0,j=0):
        """stem(word,i,j) -> string, stem string p from position i to j"""
        self._b = p
        self._i = i
        self._j = j
        if self._j <= self._i + 1: return self._b
        return self._stem()

    def _stem (self):
        abstract()


class DE_PlGen_Stemmer (ABCStemmer):
    """German plural and genus stemmer

    A simple stemmer that just removes german plural and case suffixes.
    No morphological changes (like umlauts) or prefixes.
    """
    _name_ = "DE_PlGen_Stemmer"
        
    def _stem (self):
        """"_stem() -> string, removes german plural and case suffixes

        legal plural suffixes are:
        -e, -n, -er, -en, -nen, -s

        legal case suffixes are:
        -es, -en, -em, -sen, -nen

        The string is searched right-to-left for the longest of these
        suffixes and matching characters are removed.

        The search proceeds as follows:
        -n -> -en/-en -> -sen
        -s -> -es
        -em / -er
        -e
        """
        if self._b[self._j-1] == 'n':
            self._j -= 1
            if self._b[self._j-1] == 'e':
                self._j -= 1
                if self._b[self._j-1] == 's':
                    self._j -= 1
                if self._b[self._j-1] == 'n':
                    self._j -= 1
        elif self._b[self._j-1] == 's':
            self._j -= 1
            if self._b[self._j-1] == 'e':
                self._j -= 1
        elif self._b.endswith('em') or self._b.endswith('er'):
            self._j -= 2
        elif self._b[self._j-1] == 'e':
            self._j -= 1
        return self._b[self._i:self._j]

class DE_Porter_Stemmer (ABCStemmer):
    """German Porter Stemmer Algorithm

    Based on the algorithm described in
    http://snowball.tartarus.org/algorithms/german/stemmer.html
    See above url for further information

    The german porter stemmer uses regions r1 r2 as described in
    http://snowball.tartarus.org/texts/r1r2.html
    
    Privates:
    _r1             --          r1 region, after first occurance of VC
    _r2             --          r2 region, after first VC after start of r1 
    _vowels         --          german vowel definitions
    _sendings       --          valid s-endings
    _stendings      --          valid st-endings

    The stemmer works in 3 steps which are implemented in the functions
    _step1(), _step2(), step3() and called from _stem()
    """
    _name_ = "DE_Porter_Stemmer"
    
    def __init__ (self):
        self._vowels = ['a','e','i','o','u','y',u'ä',u'ö',u'ü']
        self._sendings = ['b','d','f','g','h','k','l','m','n','r','t']
        self._stendings = ['b','d','f','g','h','k','l','m','n','t']
        # initialize regions with 0
        self._r1 = 0
        self._r2 = 0

    def _stem (self):
        # First, replace ß by ss ...
        self._b = self._b.replace(u'ß','ss')
        # ... and put u and y between vowels into upper case.
        for i in range(self._i,self._j):
            if self._b[i] in ['u','y']:
                if self._i < i < self._j-1 and self._b[i-1] in self._vowels and self._b[i+1] in self._vowels:
                    if self._b[i] == 'u': self._b[i] = 'U'
                    elif self._b[i] == 'y': self._b[i] = 'Y'
                    break
        # identify regions
        self._r1 = self._region(self._i)
        self._r2 = self._region(self._r1)
        # execute the three steps
        self._step1()
        self._step2()
        self._step3()
        # finally turn U and Y back into lower case, and remove the umlaut accent from a, o and u.
        self._b = self._b.replace('U','u').replace('Y','y').replace(u'ä','a').replace(u'ö','o').replace(u'ü','u')
        # return the stem
        return self._b[self._i:self._j]
        
    def _step1 (self):
        """_step1() -- first stemming step

        Search for the longest among the following suffixes,
        (a) e   em   en   ern   er   es
        (b) s (preceded by a valid s-ending not necessarily in R1) 
        and delete if in R1.
        """
        # local copy of self._b, faster access and less writing
        b = self._b[self._r1:self._j]
        if len(b) < 2: return
        if b.endswith('ern'):
            self._j -= 3
        elif b.endswith('em') or b.endswith('en') or b.endswith('er') or b.endswith('es'):
            self._j -= 2
        elif b[-1] == 'e':
            self._j -= 1
        elif b[-1] == 's' and self._b[self._j-2] in self._sendings:
            self._j -= 1
        
    def _step2 (self):
        """_step1() -- second stemming step

        Search for the longest among the following suffixes,
        (a) en   er   est
        (b) st (preceded by a valid st-ending, itself preceded by at least 3 letters)
        and delete if in R1. 
        """
        b = self._b[self._r1:self._j]
        if len(b) < 2: return
        if b.endswith('ern'):
            self._j -= 3
        elif b.endswith('en') or b.endswith('er'):
            self._j -= 2
        elif b.endswith('es') and self._b[self._j-3] in self._stendings and len(self._b[self._i:self._j])>= 6:
            self._j -= 2
        
    def _step3 (self):
        """_step3() -- last stemming step, d-suffixes

        Search for the longest among the following suffixes,
        and perform the action indicated.

        end   ung
            delete if in R2 
            if preceded by ig,
            delete if in R2 and not preceded by e

        ig   ik   isch
            delete if in R2 and not preceded by e

        lich   heit
            delete if in R2
            (i guess this should be r1, aengstlich -> angst, changed accordingly)
            if preceded by er or en, delete if in R1

        keit
            delete if in R2 
            if preceded by lich or ig, delete if in R2
            (should be r1 I guess)
        """
        r2 = self._b[self._r2:self._j]
        r1 = self._b[self._r1:self._j]
        if len(r1) < 2 and len(r2) < 2: return
        if r1.endswith('keit'):
            self._j -= 4
            if r1[0:-4].endswith('lich'):
                self._j -= 4
            elif r1[0:-4].endswith('ig'):
                self._j -= 2
        elif r1.endswith('lich') or r1.endswith('heit'):
            self._j -= 4
            if r1[0:-4].endswith('er') or r1[0:-4].endswith('en'):
                self._j -= 2
        elif r2.endswith('isch') and self._b[self._j-5] != 'e':
            self._j -= 4
        elif r2.endswith('end') or r2.endswith('ung'):
            self._j -= 3
            if r2[0:-3].endswith('ig') and self._b[self._j-3] != 'e':
                self._j -= 2
        elif r2.endswith('ig') or r2.endswith('ik') and self._b[self._j-3] != 'e':
            self._j -= 2
        
    def _region (self,start):
        """_definer(start) -> identifies and returns the index of r region"""
        if start == self._j: return start
        r = self._j
        for i in range(start,self._j):
            if self._b[i] in self._vowels:
                try:
                    if self._b[i+1] not in self._vowels:
                            if i+2 <= self._j:
                                r = i+2
                                break
                except (IndexError):
                    break
        return r        

class EN_Porter_Stemmer (ABCStemmer):
    """Porter Stemming Algorithm
    This is the Porter stemming algorithm, ported to Python from the
    version coded up in ANSI C by the author. It may be be regarded
    as canonical, in that it follows the algorithm presented in

    Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
    no. 3, pp 130-137,

    only differing from it at the points maked --DEPARTURE-- below.

    See also http://www.tartarus.org/~martin/PorterStemmer

    The algorithm as described in the paper could be exactly replicated
    by adjusting the points of DEPARTURE, but this is barely necessary,
    because (a) the points of DEPARTURE are definitely improvements, and
    (b) no encoding of the Porter stemmer I have seen is anything like
    as exact as this version, even with the points of DEPARTURE!

    Vivake Gupta (v@nano.com)

    Release 1: January 2001

    Further adjustments by Santiago Bruno (bananabruno@gmail.com)
    to allow word input not restricted to one word per line, leading
    to:

    release 2: July 2008

    changes:
    March 2009:  Sebastian Kay Belle
        minor changes to conform to ABCStemmer interface
        UNTESTED !
    """

    _name_ = "EN_Porter_Stemmer"
    
    def __init__(self):
        """The main part of the stemming algorithm starts here.
        b is a buffer holding a word to be stemmed. The letters are in b[k0],
        b[k0+1] ... ending at b[k]. In fact k0 = 0 in this demo program. k is
        readjusted downwards as the stemming progresses. Zero termination is
        not in fact used in the algorithm.

        Note that only lower case sequences are stemmed. Forcing to lower case
        should be done before stem(...) is called.
        """
        ABCStemmer.__init__(self)

    def cons(self, i):
        """cons(i) is TRUE <=> b[i] is a consonant."""
        if self.b[i] == 'a' or self.b[i] == 'e' or self.b[i] == 'i' or self.b[i] == 'o' or self.b[i] == 'u':
            return 0
        if self.b[i] == 'y':
            if i == self.k0:
                return 1
            else:
                return (not self.cons(i - 1))
        return 1

    def m(self):
        """m() measures the number of consonant sequences between k0 and j.
        if c is a consonant sequence and v a vowel sequence, and <..>
        indicates arbitrary presence,

           <c><v>       gives 0
           <c>vc<v>     gives 1
           <c>vcvc<v>   gives 2
           <c>vcvcvc<v> gives 3
           ....
        """
        n = 0
        i = self.k0
        while 1:
            if i > self.j:
                return n
            if not self.cons(i):
                break
            i = i + 1
        i = i + 1
        while 1:
            while 1:
                if i > self.j:
                    return n
                if self.cons(i):
                    break
                i = i + 1
            i = i + 1
            n = n + 1
            while 1:
                if i > self.j:
                    return n
                if not self.cons(i):
                    break
                i = i + 1
            i = i + 1

    def vowelinstem(self):
        """vowelinstem() is TRUE <=> k0,...j contains a vowel"""
        for i in range(self.k0, self.j + 1):
            if not self.cons(i):
                return 1
        return 0

    def doublec(self, j):
        """doublec(j) is TRUE <=> j,(j-1) contain a double consonant."""
        if j < (self.k0 + 1):
            return 0
        if (self.b[j] != self.b[j-1]):
            return 0
        return self.cons(j)

    def cvc(self, i):
        """cvc(i) is TRUE <=> i-2,i-1,i has the form consonant - vowel - consonant
        and also if the second c is not w,x or y. this is used when trying to
        restore an e at the end of a short  e.g.

           cav(e), lov(e), hop(e), crim(e), but
           snow, box, tray.
        """
        if i < (self.k0 + 2) or not self.cons(i) or self.cons(i-1) or not self.cons(i-2):
            return 0
        ch = self.b[i]
        if ch == 'w' or ch == 'x' or ch == 'y':
            return 0
        return 1

    def ends(self, s):
        """ends(s) is TRUE <=> k0,...k ends with the string s."""
        length = len(s)
        if s[length - 1] != self.b[self.k]: # tiny speed-up
            return 0
        if length > (self.k - self.k0 + 1):
            return 0
        if self.b[self.k-length+1:self.k+1] != s:
            return 0
        self.j = self.k - length
        return 1

    def setto(self, s):
        """setto(s) sets (j+1),...k to the characters in the string s, readjusting k."""
        length = len(s)
        self.b = self.b[:self.j+1] + s + self.b[self.j+length+1:]
        self.k = self.j + length

    def r(self, s):
        """r(s) is used further down."""
        if self.m() > 0:
            self.setto(s)

    def step1ab(self):
        """step1ab() gets rid of plurals and -ed or -ing. e.g.

           caresses  ->  caress
           ponies    ->  poni
           ties      ->  ti
           caress    ->  caress
           cats      ->  cat

           feed      ->  feed
           agreed    ->  agree
           disabled  ->  disable

           matting   ->  mat
           mating    ->  mate
           meeting   ->  meet
           milling   ->  mill
           messing   ->  mess

           meetings  ->  meet
        """
        if self.b[self.k] == 's':
            if self.ends("sses"):
                self.k = self.k - 2
            elif self.ends("ies"):
                self.setto("i")
            elif self.b[self.k - 1] != 's':
                self.k = self.k - 1
        if self.ends("eed"):
            if self.m() > 0:
                self.k = self.k - 1
        elif (self.ends("ed") or self.ends("ing")) and self.vowelinstem():
            self.k = self.j
            if self.ends("at"):   self.setto("ate")
            elif self.ends("bl"): self.setto("ble")
            elif self.ends("iz"): self.setto("ize")
            elif self.doublec(self.k):
                self.k = self.k - 1
                ch = self.b[self.k]
                if ch == 'l' or ch == 's' or ch == 'z':
                    self.k = self.k + 1
            elif (self.m() == 1 and self.cvc(self.k)):
                self.setto("e")

    def step1c(self):
        """step1c() turns terminal y to i when there is another vowel in the stem."""
        if (self.ends("y") and self.vowelinstem()):
            self.b = self.b[:self.k] + 'i' + self.b[self.k+1:]

    def step2(self):
        """step2() maps double suffices to single ones.
        so -ization ( = -ize plus -ation) maps to -ize etc. note that the
        string before the suffix must give m() > 0.
        """
        if self.b[self.k - 1] == 'a':
            if self.ends("ational"):   self.r("ate")
            elif self.ends("tional"):  self.r("tion")
        elif self.b[self.k - 1] == 'c':
            if self.ends("enci"):      self.r("ence")
            elif self.ends("anci"):    self.r("ance")
        elif self.b[self.k - 1] == 'e':
            if self.ends("izer"):      self.r("ize")
        elif self.b[self.k - 1] == 'l':
            if self.ends("bli"):       self.r("ble") # --DEPARTURE--
            # To match the published algorithm, replace this phrase with
            #   if self.ends("abli"):      self.r("able")
            elif self.ends("alli"):    self.r("al")
            elif self.ends("entli"):   self.r("ent")
            elif self.ends("eli"):     self.r("e")
            elif self.ends("ousli"):   self.r("ous")
        elif self.b[self.k - 1] == 'o':
            if self.ends("ization"):   self.r("ize")
            elif self.ends("ation"):   self.r("ate")
            elif self.ends("ator"):    self.r("ate")
        elif self.b[self.k - 1] == 's':
            if self.ends("alism"):     self.r("al")
            elif self.ends("iveness"): self.r("ive")
            elif self.ends("fulness"): self.r("ful")
            elif self.ends("ousness"): self.r("ous")
        elif self.b[self.k - 1] == 't':
            if self.ends("aliti"):     self.r("al")
            elif self.ends("iviti"):   self.r("ive")
            elif self.ends("biliti"):  self.r("ble")
        elif self.b[self.k - 1] == 'g': # --DEPARTURE--
            if self.ends("logi"):      self.r("log")
        # To match the published algorithm, delete this phrase

    def step3(self):
        """step3() dels with -ic-, -full, -ness etc. similar strategy to step2."""
        if self.b[self.k] == 'e':
            if self.ends("icate"):     self.r("ic")
            elif self.ends("ative"):   self.r("")
            elif self.ends("alize"):   self.r("al")
        elif self.b[self.k] == 'i':
            if self.ends("iciti"):     self.r("ic")
        elif self.b[self.k] == 'l':
            if self.ends("ical"):      self.r("ic")
            elif self.ends("ful"):     self.r("")
        elif self.b[self.k] == 's':
            if self.ends("ness"):      self.r("")

    def step4(self):
        """step4() takes off -ant, -ence etc., in context <c>vcvc<v>."""
        if self.b[self.k - 1] == 'a':
            if self.ends("al"): pass
            else: return
        elif self.b[self.k - 1] == 'c':
            if self.ends("ance"): pass
            elif self.ends("ence"): pass
            else: return
        elif self.b[self.k - 1] == 'e':
            if self.ends("er"): pass
            else: return
        elif self.b[self.k - 1] == 'i':
            if self.ends("ic"): pass
            else: return
        elif self.b[self.k - 1] == 'l':
            if self.ends("able"): pass
            elif self.ends("ible"): pass
            else: return
        elif self.b[self.k - 1] == 'n':
            if self.ends("ant"): pass
            elif self.ends("ement"): pass
            elif self.ends("ment"): pass
            elif self.ends("ent"): pass
            else: return
        elif self.b[self.k - 1] == 'o':
            if self.ends("ion") and (self.b[self.j] == 's' or self.b[self.j] == 't'): pass
            elif self.ends("ou"): pass
            # takes care of -ous
            else: return
        elif self.b[self.k - 1] == 's':
            if self.ends("ism"): pass
            else: return
        elif self.b[self.k - 1] == 't':
            if self.ends("ate"): pass
            elif self.ends("iti"): pass
            else: return
        elif self.b[self.k - 1] == 'u':
            if self.ends("ous"): pass
            else: return
        elif self.b[self.k - 1] == 'v':
            if self.ends("ive"): pass
            else: return
        elif self.b[self.k - 1] == 'z':
            if self.ends("ize"): pass
            else: return
        else:
            return
        if self.m() > 1:
            self.k = self.j

    def step5(self):
        """step5() removes a final -e if m() > 1, and changes -ll to -l if
        m() > 1.
        """
        self.j = self.k
        if self.b[self.k] == 'e':
            a = self.m()
            if a > 1 or (a == 1 and not self.cvc(self.k-1)):
                self.k = self.k - 1
        if self.b[self.k] == 'l' and self.doublec(self.k) and self.m() > 1:
            self.k = self.k -1

    def _stem(self):
        """In stem(p,i,j), p is a char pointer, and the string to be stemmed
        is from p[i] to p[j] inclusive. Typically i is zero and j is the
        offset to the last character of a string, (p[j+1] == '\0'). The
        stemmer adjusts the characters p[i] ... p[j] and returns the new
        end-point of the string, k. Stemming never increases word length, so
        i <= k <= j. To turn the stemmer into a module, declare 'stem' as
        extern, and delete the remainder of this file.
        """
        # With this line, strings of length 1 or 2 don't go through the
        # stemming process, although no mention is made of this in the
        # published algorithm. Remove the line to match the published
        # algorithm.

        self.step1ab()
        self.step1c()
        self.step2()
        self.step3()
        self.step4()
        self.step5()
        return self.b[self.k0:self.k+1]
