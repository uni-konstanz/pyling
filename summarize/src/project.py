# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Python for Linguists final project

This program reads a UTF-16 encoded input file
generates a summary and writes the results to
an output file.

Implements the following classes:

Corpus              --          reads and preprocesses an input file
Words               --          analyses the words
AutoSum             --          autogenerate the summary

"""

__all__ = ["Corpus","Words","AutoSum","umlauts","vowels","hist"]

####################################################################
# Imports
####################################################################

from re import *
from operator import *
import codecs
import sys
from abstract import *
from syllabify import *
from stemmer import *
import heapq


####################################################################
# Globals
####################################################################

umlauts = [u'ä',u'Ä',u'ö',u'Ö',u'ü',u'Ü',u'ß']
vowels = ['a','e','i','o','u','A','E','I','O','U',u'ä',u'Ä',u'ö',u'Ö',u'ü',u'Ü']
consonants = ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z']


####################################################################
# Classes
####################################################################

class Corpus (object):
    """Reads and preprocesses the input file

    Privates:
    _filename       --      the name of the input file
    _original       --      the original input string
    _normalized     --      normalized string
    _types          --      dictionary of types and their frequency
    _tokens         --      a list of tokens of the input string
    _sent           --      a list of the sentences in the input string

    Properties:
    original
    normalized
    types
    tokens
    sent
    """
    
    def __init__ (self,filename):
        """Instantiate a Corpus object

        Needs a file object to work on. The file is normalized
        and all the members are initialized.
        
        @textfile           --          the textfile instance
        @raise              --          TypeError if no file is provided
        """
        if not isinstance(filename,str):
            raise TypeError("Argument 'filename' must be a string.")
        textfile = codecs.open(filename,'r',encoding='utf-16')
        self._filename = textfile.name
        self._original = textfile.read()
        self._normalized = compile(''.join(['[^a-z',reduce(add,umlauts),']+'])).sub(' ',self._original.lower())
        origsent = compile("[!?.\\d]+ *").split(self._original.lower())[:-1]
        self._sent = [compile(''.join(['[^a-z',reduce(add,umlauts),']+'])).sub(' ',i).strip(' ') for i in origsent]
        self._tokens = self._normalized.split()
        self._types = hist(*(self._tokens))
        textfile.close()

    def __repr__ (self):
        """repr(self) -> return a string representation of the Corpus instance"""
        return "Corpus(%s)" % (self._filename)
    
    # Setter, Getter, Deleter
    # remark for python <= 2.5.*:
    #   for fget,fset,fdel functions, the same name as the member plus prefix should be used
    #   ie gettypes,settypes,deltypes. no capitalization of any characters
    # remark on python >= 2.6:
    #   the use of decorators with new keywords .setter, .getter, .deleter is encouraged
    #   see http://docs.python.org/library/functions.html for more information on python objects
    def getTypes (self):
        """getTypes() -> return a dictionary of types and their frequency { type : frequency }"""
        return self._types
    types = property(getTypes)
    
    def getTokens (self):
        """getTokens() -> return the list of tokens [ tokens ]"""
        return self._tokens
    tokens = property(getTokens)
    
    def getSent (self):
        """getSent() -> return the list of sentences [ sentences ]"""
        return self._sent
    sent = property(getSent)

    def getOriginal (self):
        """getOriginal() -> string, return the original input string"""
        return self._original
    original = property(getOriginal)

    def getNormalized (self):
        """getNormalized() -> string, return the normalized input string"""
        return self._normalized
    normalized = property(getNormalized)
    
    # Other Functions
    def printStats (self):
        """printStats() -- print statistical information on stdout

        The following information is claculated and printed out:
        number of types
        number of tokens
        number of sentences
        average typelength
        average tokenlength
        average sentencelength
        """
        stats = self._calcstats()
        output = "Number types: %s\nNumber tokens: %s\nNumber sentences: %s\nAverage typelength: %s\nAverage tokenlength: %s\nAverage sentencelength: %s" % stats
        #Print (output)
        print output
        return output

    def _calcstats (self):
        """_calcstats() -> calculate and return statistical information"""
        # compute the lengths
        # local access is much more efficient
        # for further processing than returning function calls
        typekeys = self._types.keys()
        numtypes = len(typekeys)
        numtokens = len(self._tokens)
        numsent = len(self._sent)
        # compute averages
        avgtypelen = sum(map(len,typekeys)) / float(numtypes)
        avgtokenlen = sum(map(len,self._tokens)) / float(numtokens)
        avgsentlen = numtokens / float(numsent)
        return (numtypes,numtokens,numsent,avgtypelen,avgtokenlen,avgsentlen)
        

class Words (object):
    """Analyze the words of the corpus

    Privates:
    _syllables             --          dict of words and corresponding syllables
    _stems                 --          dict of words and their stem

    Properties:
    syllables              --          property of _syllables
    stems                  --          property of _stems
    """

    def __init__ (self,types,auto=False,adv=False):
        """Instantiate a Words object

        On instantiation syllables and stems are computed by using
        the corresponding functions.

        @types          --          a dictionary of types and their frequency
        @auto           --          if true use autoSyllabify
        @adv            --          if true use advancedStemming
        @raise          --          TypeError
        """
        if type(types) != type({}):
            raise TypeError("Argument 'types' must be of type dictionary")
        self._types = types
        # Remarks on syllabification and stemming:
        # It would be cleaner to provide one function for each syllabify() and stem()
        # and set Syllabifier and Stemmer objects during instantiation.
        # This would allow for more robust and expandable code.
        # However, since the Words interface is fixed we set function pointers
        # accordingly and instantiate the different objects in the functions
        syllabify = self.autoSyllabify if auto else self.syllabify
        stemming = self.advancedStemming if adv else self.stemming
        self._syllables = syllabify()
        self._stems = stemming()

    def __repr__ (self):
        """repr(self) -> return a string representation of the Words instance"""
        return "Words(%s)" % (id(self))
  
    def _getsyllables (self):
        """_getsyllables() -> return self._syllables"""
        return self._syllables
    syllables = property(_getsyllables)
    
    def _getstems (self):
        """_getstems() -> return self._stems"""
        return self._stems
    stems = property(_getstems)
    
    def getSyll (self,word):
        """getSyll(word) -> return a list of syllables of word"""
        return self._syllables.get(word,None)
  
    def getStem (self,word):
        """getStem(word) -> return the stem of word"""
        return self._stems.get(word,None)

    def syllabify (self):
        """syllabify(*words) -> { word : [syllables] }

        for each word the syllables are split right before the consonant
        prior to the last vowel.

        @words          --          a list of words
        """
        return SimpleSyllabifier(*vowels).__call__(*(self._types.keys()))
    
    def autoSyllabify (self):
        """autoSyllabify() -- automagically identify syllable borders"""
        return AutoSyllabifier(*vowels).__call__(*(self._types.keys()))

    def stemming (self):
        """stemming() -- stems the input words

        simple stemming that only removes german plural suffixes
        """
        return DE_PlGen_Stemmer().__call__(*(self._types.keys()))
        
    def advancedStemming(self):
        """advancedStemming() -- use porter stemmer to stem the words"""
        return DE_Porter_Stemmer().__call__(*(self._types.keys()))

class AutoSum (object):
    """Summary class

    Automatically generates the summary as proposed in
    H.P. Luhn,"The Automatic  Creation of Literature Abstracts",1958

    The algorithm is twofold. First significant words are computed, then significant
    sentences identified.

    Compute significant words:
    Stopwords are removed from types
    Remaining types are stemmed
    Frequency of the stemmed types is computed
    _threshold most frequent stems are significant

    Compute significant sentences:
    each word of each sentence is analyse for significance.
    significant words not more than 4 words apart are clustered
    significance factor = (num sig words in cluster)**2 / (num words in cluster)

    Privates
    _nrSentences            --          number of sentences of the summary
    _threshold              --          number of significant words
    _corpus                 --          the corpus to summarize
    _words                  --          a words instance of the types in _corpus
    _stopwords              --          a list of stopwords
    _stems                  --          a dictionary of stems and their frequency
    _sentences              --          a dictionary of sentences and their significance factor
    _window                 --          window size for computing word clusters

    Properties::
    nrSentences
    threshold
    """
    
    def __init__ (self,corpus,nrSentences,threshold,stopwords='stopwords.txt',window = 4,auto=False,adv=False):
        """Instantiate an AutoSum object

        @corpus             --          a legal Corpus instance
        @nrSentences        --          the number of significant sentences
        @threshold          --          fraction of significant words
        @stopwords          --          either None, for automatic stopwords or a filename
        @auto               --          passed to Words
        @adv                --          passed to Words
        """
        if not isinstance(corpus,Corpus):
            raise TypeError("Argument 'corpus' must be a Corpus instance")
        if stopwords != None and type(stopwords) != type(''):
            raise TypeError("Argument 'stopwords' must be either None or a legal filename")
        self._corpus = corpus
        self._words = Words(self._corpus._types,auto,adv)
        self._nrSentences = nrSentences
        self._threshold = threshold
        self._stopwords = self.getStopwords(stopwords) if stopwords != None else self.getAutoStopwords()
        self._window = window
        self._stems = self._computeStemFreq()
        self._sentences = self._computeSigSent()
        
    def _getnrSentences (self):
        """_getnrSentences() -> self._nrSentences"""
        return self._nrSentences

    def _setnrSentences (self,x):
        """_setnrSentences(x) -- self._nrSentences = x"""
        self._nrSentences = x
        
    nrSentences = property(_getnrSentences,_setnrSentences)

    def _getthreshold (self):
        """_getthreshold() -> self._threshold"""
        return self._threshold
    
    def _setthreshold (self,x):
        """_setthreshold() -- self._threshold = x"""
        self._threshold = x

    threshold = property(_getthreshold,_setthreshold)

    def _computeStemFreq (self):
        """_computeStemFreq() -> { stem : frequency }, compute stem frequencies"""
        # remove stopwords and calculate stem frequency
        stems = {}
        for i in self._corpus.getTokens():
            if i not in self._stopwords:
                stem = self._words.getStem(i)
                try: stems[stem] += 1
                except (KeyError): stems[stem] = 1
        return stems

    def _computeSigSent (self):
        """_computeSigSent() -> { sent : sigfactor }, computes the significance factor of each sentence"""
        sentences = {}
        sigwords = self.getSigWords()
        # check every sentence
        for sent in self._corpus.getSent():
            stems = [self._words.getStem(i) for i in sent.split()]
            found = False
            numwords = 0
            numsig = 0
            dist = 0
            maxwords = 0
            sfactor = 0.0
            for stem in stems:
                if stem in sigwords:
                    found = True
                    numwords += (1 + dist)
                    numsig += 1
                    dist = 0
                elif found:
                    dist += 1
                    if dist >= self._window:
                        if numwords >= maxwords:
                            maxwords = numwords
                            sfactor = max(sfactor,(numsig**2/float(numwords)))
                        found = False
                        numwords = 0
                        numsig = 0
                        dist = 0
            sentences[sent] = sfactor
        return sentences
                        
    def getStopwords (self, filename='stopwords.txt'):
        """getStopwords() -- read the stopword list

        if no filename is provided asumes a stopword file
        named stopwords.txt in working directory.
        The provided stopwords.txt has been taken from:
        http://snowball.tartarus.org/algorithms/german/stop.txt
        some words have been added.
        A stopword file must contain one word in every line.
        """
        return [line[:-1] for line in codecs.open(filename,'r',encoding='utf-8')]
        
    def getSigWords (self):
        """getSigWords() -> return a dictionary of significant words and their frequency"""
        return dict(heapq.nlargest(self._threshold,self._stems.iteritems(),itemgetter(1)))
        
    def getSigSent (self):
        """getSigSent() -> return significant sentences"""
        return dict(heapq.nlargest(self._nrSentences,self._sentences.iteritems(),itemgetter(1)))

    def printSummary (self,filename=''):
        """printSummary() -- print the summary on stdout and to an optional file"""
        self._corpus.printStats()
        print
        sigsents = self.getSigSent()
        for i in sigsents:
            iout = "%s, (%s)\n" % (i,self._sentences[i])
            sys.stdout.write(iout.encode("utf-8"))
        if filename != '':
            output = ''
            for sent in sigsents:
                # check if sentence fits into line
                output += self._splitsent(sent,70) + '\n' + "(%s)" % self._sentences[sent] + '\n'
            # finally write file
            outfile = open(filename,'w')
            outfile.write(output.encode("utf-8"))
            outfile.close()

    def _splitsent (self,s,m):
        """_splitsent(s,m) -> string, splits a sentence s such that each line has max m characters"""
        lines = ''          # the split lines
        carry = ''          # carry of words or syllables
        coff = 0            # carry offset pointer into sentence
        # while there are still characters left
        while len(s) > 0:
            lines += carry
            offset = m-len(carry)
            if len(s) <= offset:
                lines += s
                carry = ''
                coff = 0
                s = ''
            # if the sentence has to be split within a word
            elif s[offset] != ' ':
                # identify word and get syllable
                left = s[:offset].rfind(' ')
                right = s[offset:].find(' ')
                word = s[left+1:offset+right]
                syls = '-'.join(self._words.getSyll(word))
                # append substring to line
                lines += s[:left+1]
                # find inword split point
                charsleft = offset - left - 1
                breakpoint = charsleft
                # if split point is between syllables
                if syls[breakpoint] == '-':
                    rest = syls[breakpoint+1:len(syls)].replace('-','')
                    carry =  rest
                    lines += syls[:breakpoint+1].replace('-','') + '-' + '\n'
                    coff = len(word[breakpoint:])
                else:
                    sleft = syls[:breakpoint].rfind('-')
                    # no splitpoint left from breakpoint
                    if sleft == -1:
                        lines += '\n'
                        carry = word
                        coff = len(word[breakpoint:])
                    # word can be split
                    else:
                        lines += syls[:sleft+1].replace('-','') + '-' + '\n'
                        carry =  syls[sleft+1:len(syls)].replace('-','')
                        coff = len(word[breakpoint:])
            # if no word must be split, only split sentence
            elif s[offset] == ' ':
                lines += s[:offset] + '\n'
                carry = ''
                coff = 1
            s = s[offset+coff:]
        return lines
        
    def getAutoStopwords (self):
        """getAutoStopwords() -- automatically identify stop words

        To identify stopwords usually the TF-IDF is used.
        However, since there is only one document -> idf = log 1 = 0.
        Thus only TF is computed and compared to the average TF.
        In addition word length is taken into account and
        compared to average word length. Those words
        that have both below average TF and len are
        considered insignificant and classified as stopwords.
        """
        numtypes = float(len(self._corpus._types))
        # compute average TF and type len
        sumtf = sum([self._corpus._types[i]/numtypes for i in self._corpus._types])
        avgtf = sumtf / sumtf
        avglen = int(sum(map(len,self._corpus._types)) / numtypes)
        # compute stopwords
        stopwords = []
        for term in self._corpus._types:
            tf = self._corpus._types[term]/numtypes
            if len(term) < avglen and tf < avgtf:
                stopwords.append(term)
        return stopwords
            
        
####################################################################
# FUNCTIONS
####################################################################

def hist (*args):
    """hist(*args) -> { arg : frequency }"""
    h = {}
    for a in args:
        try: h[a] += 1
        except (KeyError): h[a] = 1
    return h

####################################################################
# MAIN
####################################################################
def test (argv):
    filename = raw_input("Filename: ") or 'rotkaeppchen.txt'
    c = Corpus(filename)
    print "\nSTANDARD PROCEDURE"
    a1 = AutoSum(c,5,15)
    a1.printSummary('b')
    print "\nAUTOMATIC PROCEDURE"
    a2 = AutoSum(corpus = c,nrSentences = 5,threshold = 15,stopwords=None,adv=True)
    a2.printSummary('b')
    return a1,a2

if __name__ == '__main__':
    a1,a2 = test(sys.argv[1:])
