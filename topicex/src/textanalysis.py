# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

'''
preparation.py calculates from a list of documents the corresponding vectors
'''
import porterstemmer, term, vec, sys
'''
the method setStopWord creates a list of stopwords from a file
@param name the stopword file
@return a list of all stopwords
'''
def setStopWord(name):
  in_file = open(name,'r')
  stopword = {}
  for line in in_file:
    stopword[line[0:len(line)-1]]=1
  in_file.close()
  return stopword

'''
getAllWords walks through all documents and stores all relevant words.
@param stopword the list of stopwords
@param allDocs the list of all documents 
@param porterstemmer the instance of porterstemmer
@return a dictionary of words with its amount in all documents
'''
def getAllWords(stopword,allDocs,porterstemmer):
  wordlist = {}
  tmp = 1
  for news in allDocs:
    line_split = news.split()
    for words in line_split:
      if not (stopword.has_key(words)):
        words = porterstemmer.stem(words,0,len(words)-1)
        if (len(words)>1) and not (stopword.has_key(words)):
          if (words.find('\'')>-1):
            print "!"+words+"!"
          temp = wordlist.get(words,0)
          wordlist[words]=temp+1
  return wordlist  

'''
slimList decreases the number of dimensions.
It deletes terms, which document frequency  is too low.
@param wordsdict the dictionary with all terms
@param minCount the document frequency threshold.
@return the slimmed dictionary of terms
'''
def slimList(wordsdict,minCount):
  newDic = {}
  words_tok = wordsdict.items()
  for elements in wordsdict:
    if not wordsdict[elements]<=2:
      newDic[elements]=wordsdict[elements]
  return newDic

'''
calcVec calculates the corresponding vectors of the given documents.
@param allDocs the list of all documents
@param alltitle the list of all corresponding titles of the documents in allDocs
@param impWords a list of all terms who are relevant for the vector
@param ps porterstemmer object
@return documents is the list of all document objects where the vectors are stored
@return dicts is the dictionary of all terms 
'''
def calcVec(allDocs,alltitle,impWords,ps):
  count_dimension = 0                               # Count which dimension is actual
  count_docu = 0
  count = -1
  documents = list()
  dicts = {}  
  for news in allDocs:                            # take every document file
      count+=1
      ff = alltitle[count]
      acdoc=term.document(count_docu,(len(impWords)),ff) # create new term for acutal document
      count_docu+=1                                   # i remember how many documents i have
      documents.append(acdoc)                         # insert acutal document to list
      news_split = news.split()                 
      for words in news_split:                        # every word in a news    
        words = words.rstrip("\"\'.,?!;-\(\)<>").lower()
        words = ps.stem(words,0,len(words)-1)
        if (impWords.has_key(words)):                 # check if the stemmed word is in our "wanted" list
          curr = dicts.get(words,term.term("",-1,-1)) # if yes, look if i already know the the term
          if (curr.frequency==-1):                    # if no i create a new one
            dicts[words]=term.term(words,1,count_dimension)
            count_dimension+=1
          else:                                       # if yes i just raise the frequence 
            dicts[words].raisefrequency()                  # then i increase the dimension on that vector
          acdoc.addElement((dicts.get(words)).position)
          if (acdoc.getDim((dicts.get(words)).position)==1): # if the found one is the first time in this document rais number of docs, it occurs
            dicts[words].raisedoccount()
  return documents,dicts

'''
reduceDimension reduces the dimensions of the vectors by removing all terms that are 
in less than minPercent or more than maxPercent documents
@param minPercent is minimum threshold
@param maxPercent is maximum threshold
@param numDocs is the number of documents
@param dicts is the dictionary with all terms
@param impWords is the list of all important terms
'''
def reduceDimension(minPercent,maxPercent,numDocs,dicts,impWords):
  tmp = dicts.items()
  for element in tmp:
    factor = (element[1].documentcount)/100.0
    if (factor < minPercent) or (factor > maxPercent):
      xy = element[1].wordstem
      del impWords[xy]

'''
getMainWords examines the top terms of each document
@param top number of terms per document vector
@param docs is the list of document objects
@param impWords is the list of important terms
@return tmp is the list of all important terms, which are at least in one document at the top freq.
'''
def getMainWords(top,docs,impWords):
  tmp = {}
  for doc in docs:
    vec = doc.vector
    tmp2 = cutVec(vec,top)
    for els in tmp2:
      tmp3 = ((impWords.items())[els[0]])[0]
      count = tmp.get(tmp3,0)
      tmp[tmp3]=count+1
  return tmp

'''
cutVec cuts the vector to the most frequent dimensions
@param vec is the vector of one document
@param top number of terms for every document
@return a list of the most frequent dimension of a vector
'''
def cutVec(vec,top):
  xy = {}
  count=0
  for element in vec:
    xy[count]=element
    count+=1
  token_list = xy.items()  
  token_list.sort(key=lambda x:x[1], reverse=True)
  token_list = token_list[0:top]
  return token_list

'''
weightvectors weighs the documentvectors using the tfidf.
@param terms: term dictionary with all terms
@param documentvectors: all the document vectors to weigh
@param progress: if True output is given on console
'''
def weightvectors(terms, documentvectors, progress = False):
    n = len(documentvectors)
    values = terms.values()
    for t in values:
        i = t.position
        df = t.documentcount
        for dv in documentvectors:
            tf = dv[i]
            dv[i] = int(round(term.tfidf(tf,n,df),0))
        if progress: print '.',; sys.stdout.softspace = 0

'''
makevectors creates a list of weighted document vectors
and a list of documents with 0-length vectors
@param documents: the list of documents
@param terms: the terms dictionary
@param progress: if True some output is given 
'''
def makevectors(documents, terms, progress = False):
    documentvectors = []
    nullvectors = []
    for d in documents:
        v = vec.Vec().fromlist(list(d.vector))
        v.reference(d.id)
        if v.norm() != 0: 
            values = terms.values()
            for t in values:
                tf = v[t.position]
                df = t.documentcount
                n = len(documents)
                v[t.position] = int(round(term.tfidf(tf,n,df)))
            documentvectors.append(v)
        else: nullvectors.append(v)
        if progress: print '.',; sys.stdout.softspace = 0
    return documentvectors, nullvectors

'''
main represents the preparation part. It should be called from outside.
It gets a list of documents and calculates the corresponding vectors
@param allDocs is the list of all documents
@param alltitle is the list of corresponding title
@param file_stopwords is the file where the stopwords are stored
@return vecs is the list of all document objects with its vectors
@return dicts is the dictionary with all improtant terms
'''
def main(allDocs,alltitle,file_stopwords, progress=False):
  ps = porterstemmer.PorterStemmer()
  if progress: print "number of documents: "+str(len(allDocs))
  stopword = setStopWord(file_stopwords)                            # get the stopwordlist
  impWords = getAllWords(stopword,allDocs,ps)                       # extract all important terms
  if progress: print "number of words: "+str(len(impWords))
  minCount = (len(allDocs)/100.0)*5.0
  impWords = slimList(impWords,minCount)                            # reduce important terms whose frequence is too less
  vecs,dicts = calcVec(allDocs,alltitle,impWords,ps)                # first round of creating document objects with corresponding vectors 
  reduceDimension(0.05,0.9,len(allDocs),dicts,impWords)             # reduce again the dimension of terms who occur to often or to less
  if progress: print "number of words after reduction: "+str(len(impWords))
  vecs,dicts = calcVec(allDocs,alltitle,impWords,ps)                # second round of creating document objects with new important terms list
  dicts = None
  impWords = getMainWords(25,vecs,impWords)                         # extract important terms which occur at least in one document at top frequency 
  vecs = None
  if progress: print "number of words after frequency thresholding: "+str(len(impWords))  
  vecs,dicts = calcVec(allDocs,alltitle,impWords,ps)                # third round of creating document objects and the dictionary. This is the final result
  if progress:
      print ""
      #for doc in range(len(vecs)):
      #    print vecs[doc]
      #print ""
      print "creating document vector list",
  docvecs, nulvecs = makevectors(vecs, dicts)
  print " Done!"
  #if progress: 
  #    print "!"
  #    print "documents with 0-length vectors:",
  #    for nv in nulvecs:
  #        print str(nv.ref) + ',',; sys.stdout.softspace = 0
  #    print ""

  return vecs,dicts,docvecs,nulvecs
