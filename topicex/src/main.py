# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""
main file for the implementation of the topic extraction method proposed by
Hanan Ayad and Mohamed S. Kamel.
"Topic discovery from text using aggregation of different clustering methods."
the implementation is split into four distinct parts

1) PARSER
parser.py implements the method parseDocs() which reads documents from files
and creates a representation for further processing. It is optimized
for the Reuters collection, however any personal parser.py optimized for any
document collection will do. So for a personal collection provide a personal
parser.py file that implements parseDocs().

2) TEXT ANALYSIS
textanalysis.py
from the document representation generated in 1) the text analysis extracts
all occuring terms, performs dimension/term reduction and produces a term
dictionary, a list of all documents with term vectors, a list of weighted
vectors for each document (weighted by tfidf) suitable for clustering and a
list of null vectors for documents which have no point in the feature space
 
3) CLUSTERING
cluster.py
the document vectors generated in 2) are clustered by different clustering
algorithms. A list of clusters is generated. Each cluster is a list of vectors.

4) TOPIC EXTRACTION
topicextract.py
for every cluster the weights of every term are recalculated using a cluster specific
version of the tfidf. Terms are ordered by weight. For every cluster a user specified
number of weighted terms serve as topic. Topics describe the cluster content.
"""
import parse,textanalysis,porterstemmer,term,cluster,vec,topicextract
###
# defines
###
'''
set all your variables here
'''
# path and filenames
path = '..//res//'
outpath = '..//output//'
file_stopwords = 'stop_words.txt'
file_documents = list()
file_documents.append(path+'reut2-021.sgm')

# clustering related, see cluster.py for details
dist = vec.cos
comp = 1
leader_threshold = 0.1
kmeans_factor = 0.5
agglomorate_factor = 0.4
linkage = 'a'
aggregate_factor = 0.1

# topic related
topic_length = 5

# common
progress = True

###
# main
###
def main():
    '''
    prepare input files for automatic text analysis
    '''
    print "PARSING"
    print "extracting contents and titles from file(s)."
    contents,titles = parse.parseDocs(file_documents)
    print ""
    '''
    automatic text analysis
    '''
    print "AUTOMATIC TEXT ANALYSIS"
    documents,terms, docvecs, nulvecs = textanalysis.main(contents,titles,path+file_stopwords, progress)
    print ""
    '''
    clustering
    '''
    print "CLUSTER ANALYSIS"
    print "\nleader clustering"
    print "*: vector forms a new centroid, .: the vector is assigned to existing centroid"
    cleader = cluster.leader(docvecs,leader_threshold,dist,comp,progress)
    print "!"
    print "found " + str(len(cleader)) + " clusters"

    print "\nkmeans clustering"
    print "*: cluster is altered, .: cluster not changed, |: next iteration"
    k = int(round(kmeans_factor*len(cleader),0))
    ckmeans = cluster.kmeans(docvecs, k, dist, comp, True)
    print "!"
    print "found " + str(len(ckmeans)) + " clusters"

#    print "\nagglomorative clustering",
#    t = cluster.threshold(docvecs, agglomorate_factor, dist)
#    clink = cluster.agglomorate(docvecs, linkage, t, dist, comp, True)
#    print "!"
#    print "found " + str(len(clink)) + " clusters"

    print "\naggregate the clustering"
    print "a: consider next A cluster, b: consider next B cluster, !: aggregate"
    t = cluster.threshold(docvecs, aggregate_factor, dist)
    clustering = cluster.aggregate2(cleader,ckmeans,t,dist,comp,True) 
    print "!"
    print "found " + str(len(clustering)) + " clusters"

    print "\nhandle overlap between clusters",
    clustering = cluster.prune(docvecs,clustering,dist,comp)
    print "!"
    print "found " + str(len(clustering)) + " clusters"
    
    print ""
    '''
    topic extraction
    '''
    print "TOPIC EXTRACTION"
    topics = topicextract.FindTopics(terms, clustering, documents, topic_length, progress)
    print ""
    print "The following topics have been found.\n"
    print "One topic per cluster. A Topic is a list of Terms with their cluster weights.\n"
    for t in range(len(topics)): print "Topic["+str(t)+"]:", topics[t], "\n"
    print "\nDONE!"

    for i in range(len(clustering)):
        clustering[i].settopic(topics[i])
    
    # output to files
    out_file = open(outpath+'documents.txt','w')
    for i in documents:
        print>>out_file, i
    out_file.close()
    
    out_file = open(outpath+'terms.txt','w')
    for i in terms:
        print>>out_file, i
    out_file.close()
    
    out_file = open(outpath+'clusters.txt','w')
    for i in range(len(clustering)):
        c = clustering[i]
        for j in c:
            print>>out_file, str(i) + ":" + str(j.ref)
    out_file.close()
    
    out_file = open(outpath+'topics.txt','w')
    for i in topics:
        print>>out_file, i
    out_file.close()
    
    return documents, terms, docvecs, nulvecs, clustering, topics


if __name__ == "__main__":
    documents, terms, docvecs, nulvecs, clustering, topics = main()

