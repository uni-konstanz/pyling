# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

import random

def vec(*v):
    return Vec(*v)

class Vec:

    def __init__(self, *v):
        self.v = list(v)

    def fromlist(self, v):
        if not isinstance(v, list): raise TypeError
        self.v = v[:]
        return self
    
    def __repr__(self):
        return '<' + repr(self.v)[1:-1] + '>'

    def __str__(self):
        return '<' + repr(self.v)[1:-1] + '>'

    def __len__(self):
        return len(self.v)

    def __getitem__(self, i):
        return self.v[i]

    def __setitem__(self,i,v):
        self.v[i]=v

    def __add__(self, other):
        # element-wise addition
        v = map(lambda x, y: x+y, self, other)
        return Vec().fromlist(v)

    def __sub__(self, other):
        # element-wise subtraction
        v = map(lambda x, y: x-y, self, other)
        return Vec().fromlist(v)

    def __mul__(self, scalar):
        # multiply by scalar
        v = map(lambda x: x*scalar, self.v)
        return Vec().fromlist(v)

    def __div__(self, scalar):
        # divide by scalar
        v = map(lambda x: x/float(scalar), self.v)
        return Vec().fromlist(v)

    def __pow__(self, scalar):
        # power by scalar
        v = map(lambda x: x**scalar, self.v)
        return Vec().fromlist(v)

    def __abs__(self):
        # element-wise absolute value
        v = map(lambda x: abs(x), self.v)
        return Vec().fromlist(v)

    def __cmp__(self,other):
        return cmp(self.v, other.v)

    def __hash__(self):
        return hash(self.__repr__())

    def norm(self):
        # calculate the length/norm of the vector
        return sum(map(lambda x: x**2, self.v))**.5

    def unit(self):
        # calculate the unit vector with norm 1
        n = self.norm()
        v = map(lambda x: x/n, self.v)
        return Vec().fromlist(v)

    def dot(self, other):
        # dot product of two vectors
        return sum(map(lambda x,y: x*y, self, other))

    # appends a new dimension, with weight 1,
    # or increase the weigt of that dimension by 1 if already exist
    def appends(self, v):
        if (len(self.v)>v):
            self.v[v]=self.v[v]+1
        else:
            self.v.append(1)
    
    def fill(self,v):
        while (len(self.v)<v):
            self.v.append(0)

    # set a reference for the vector
    # which is represented
    def reference(self,ref):
        self.ref = ref
    

#######################
# helper methods
#######################

def avg(sequence):
    return sum(sequence)/float(len(sequence))

def mean(vectors):
    s = Vec().fromlist([0]*len(vectors[0]))
    for v in vectors:
        s += v
    return s/float(len(vectors))

# create a random vector with n elements
# and lower and upper bounds
def randomVec(n, lower, upper):
   v = []
   for i in range(n): v.append(int(round(random.uniform(lower,upper),0)))
   return Vec().fromlist(v)

#######################
# distance functions
#######################

def cos(v0,v1):
    return v0.dot(v1)/(v0.norm()*v1.norm())

def euclid(v0,v1):
    return sum((v0-v1)**2)**.5

def manhattan(v0,v1):
    return sum(abs(v0-v1))

def maximum(v0,v1):
    return max(abs(v0-v1))

def lmetric(v0,v1,p):
    return sum(abs(v0-v1)**p)**(1/float(p))

def delta(v0,v1):
    return sum(map(lambda x,y: 0 if x==y else 1, v0, v1))

#######################
# main
#######################

def main():
    n,lower,upper = 5,-10,10
    #a = randomVec(n,lower,upper)
    #b = randomVec(n,lower,upper)
    a = vec(5,3,8,7,5,3)
    b = vec(9,4,6,3,1,8)
    print "a:", a
    print "b:", b
    print "a==vec(5,3,8,7,5,3):", a == vec(5,3,8,7,5,3)
    print "a+b =", a+b
    print "a-b =", a-b
    print "a.dot(b) =", a.dot(b)
    print "a*3 =", a*3.0
    print "b/2 =", b/2.0
    print "sum(a) =", sum(a)
    print "avg(b) =", avg(b)
    print "a.norm() =", a.norm()
    print "b.unit() =", b.unit()
    print "mean(a,b) =", mean([a,b])
    print "cos(a,b) =", cos(a,b)
    print "euclid(a,b) =", euclid(a,b)
    print "manhattan(a,b) =", manhattan(a,b)
    print "maximum(a,b) =", maximum(a,b)
    print "delta(a,b) =", delta(a,b)
    
if __name__ == "__main__": main()
